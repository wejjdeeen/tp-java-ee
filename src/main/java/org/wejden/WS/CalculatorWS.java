package org.wejden.WS;

import javax.jws.WebMethod;
import javax.jws.WebService;

@WebService
public class CalculatorWS {

	@WebMethod
	public int add(int a, int b)
	{
		
		return a+b;
	}
}
